package com.newsapp;

import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class CalculatorModule extends ReactContextBaseJavaModule {


    CalculatorModule(ReactApplicationContext context) {
        super(context);
    }


    //    module name
    @NonNull
    @Override
    public String getName() {
        return "CalculatorModule";
    }


    @ReactMethod
    public void add(int n1, int n2, Callback callBack) {
        Log.d("CalendarModule", "Create event called with name: " + n1
                + " and location: " + n2);

        callBack.invoke(n1 + n2);


    }
}
