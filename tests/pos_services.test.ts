import {PostServices} from "../services/pos_services";


var postServices: PostServices


beforeEach(() => {
    postServices = new PostServices()
});


describe("Test post service", () => {
    test("test latest posts ", () => {

        // expect(2+2).toEqual(4)

        return postServices.getLatestPost().then(posts => {
            expect(posts).not.toBeNull()
        }).catch(err => {

        })
    })


    test("test all posts ", () => {

        // expect(2+2).toEqual(4)

        return postServices.getAllPosts().then(posts => {
            expect(posts.length).toBeGreaterThan(10)
        }).catch(err => {

        })
    })

})


