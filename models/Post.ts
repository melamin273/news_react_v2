export class Post {

    id: number
    body: string
    title: string
    imageUrl: string


    constructor(id: number, body: string, title: string, imageUrl: string) {
        this.id = id;
        this.body = body;
        this.title = title;
        this.imageUrl = imageUrl;
    }
}