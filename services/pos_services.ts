import {Post} from "../models/Post";


export class PostServices {


    async getLatestPost(): Promise<Post> {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts');

        const posts = await response.json();
        return posts.map((post: any) => <Post>{
            id: post["id"],
            body: post["body"],
            title: post["title"],
            imageUrl: "https://picsum.photos/640/360"
        })[0]
    }

    async getAllPosts(): Promise<Array<Post>> {

        const response = await fetch('https://jsonplaceholder.typicode.com/posts');

        const posts = await response.json();
        return posts.map((post: any) => <Post>{
            id: post["id"],
            body: post["body"],
            title: post["title"],
            imageUrl: "https://picsum.photos/640/360"
        })


    }

}

