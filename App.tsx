/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {NavigationContainer} from '@react-navigation/native';


import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomePage from './screens/MainPage';
import NewsDetailScreen from "./screens/NewsDetailScreen";
import {Post} from "./models/Post";
import createStackNavigator from "react-native-screens/createNativeStackNavigator";
// import { createStackNavigator } from '@react-navigation/stack';
import {RootStackParamList} from "./screens/RootStackPrams";
import {Provider} from "react-redux";
import {store} from "./screens/state_managment/store";

const Stack = createNativeStackNavigator();

export const ListTypeContext = React.createContext(true);



const MyTheme = {
    dark: false,
    colors: {
        primary: 'rgb(255, 45, 85)',
        background: 'rgb(242, 242, 242)',
        card: 'rgb(255, 255, 255)',
        text: 'rgb(28, 28, 30)',
        border: 'rgb(199, 199, 204)',
        notification: 'rgb(255, 69, 58)',
    },
};

const App = () => {
    return (
        <Provider store={store}>
            <NavigationContainer theme={MyTheme}>
                <Stack.Navigator>
                    <Stack.Screen
                        name="MainHome"
                        component={HomePage}
                        options={{headerShown: false}}
                    />
                    <Stack.Screen name="NewsDetail" component={NewsDetailScreen}/>
                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    );
};

// const styles = StyleSheet.create({
//   sectionContainer: {
//     marginTop: 32,
//     paddingHorizontal: 24,
//   },
//   sectionTitle: {
//     fontSize: 24,
//     fontWeight: '600',
//   },
//   sectionDescription: {
//     marginTop: 8,
//     fontSize: 18,
//     fontWeight: '400',
//   },
//   highlight: {
//     fontWeight: '700',
//   },
// });

export default App;
