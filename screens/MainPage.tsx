import React from "react";

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeFragment from "./BottomNav/HomeFragment";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CategoriesFragment from "./BottomNav/CategoriesFragment";
import SearchFragment from "./BottomNav/SearchFragment";
import ArchiveFragment from "./BottomNav/ArchiveFragment";
import {RootStackParamList} from "./RootStackPrams";

const Tab = createBottomTabNavigator(); //*


// const Tab = createBottomTabNavigator();

const HomePage: React.FC<{ title: String; }> = ({title}) => {
    return (
        <Tab.Navigator
            screenOptions={{
                tabBarShowLabel: false
            }}
        >
            <Tab.Screen options={{
                tabBarIcon: ({color, size}) => (
                    <Icon name="trending-down" size={size} color={color}/>

                ),
            }} name="Home" component={HomeFragment}/>

            <Tab.Screen name="Category" component={CategoriesFragment}
                        options={{
                            tabBarIcon: ({color, size}) => (
                                <Icon name="apps" size={size} color={color}/>

                            ),
                        }}
            />
            <Tab.Screen name="Search" component={SearchFragment}
                        options={{
                            tabBarIcon: ({color, size}) => (
                                <Icon name="shield-search" size={size} color={color}/>

                            ),
                        }}
            />
            <Tab.Screen name="Archive" component={ArchiveFragment}
                        options={{
                            tabBarIcon: ({color, size}) => (
                                <Icon name="archive-outline" size={size} color={color}/>

                            ),
                        }}
            />
        </Tab.Navigator>
    );
}


export default HomePage;

