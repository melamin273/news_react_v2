import {PostServices} from "../../../services/pos_services";
import {Action, ActionCreator} from "redux";
import {Post} from "../../../models/Post";
import {ThunkAction} from "redux-thunk";
import {Store} from "../types";


let postServices = new PostServices()



export const SET_LATEST_NEWS : string = "SET_LATEST_NEWS";
export const SET_ALL_NEWS  : string = "SET_ALL_NEWS";


export type ActionTypes =
    | { type: typeof SET_ALL_NEWS; payload: Post[] }
    | { type: typeof SET_LATEST_NEWS; payload: Post }
    ;
export const setAllNews = (payload: Post[]): ActionTypes => ({type: SET_ALL_NEWS, payload: payload});
export const setLatestNews = (payload: Post): ActionTypes => ({type: SET_LATEST_NEWS, payload: payload});

export const getAllNews = (
): ThunkAction<void, Store, unknown, Action<string>> => async (dispatch) => {

    let post = await postServices.getAllPosts()

    console.log(post.length)


    dispatch(setAllNews(post));
};

export const getLatestNews = (

): ThunkAction<void, Store, unknown, Action<string>> => async (dispatch) => {

    let posts = await postServices.getLatestPost()


    dispatch(setLatestNews(posts));
};


