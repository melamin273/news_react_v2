import {Store} from "../types";
import {ActionTypes} from "./actions";
import {Reducer} from "redux";
import {Post} from "../../../models/Post";


export default function newsReducer(
    state: Store = {
        newsReducer: {
            newTodo: "",
            posts: [],
            latestPost: new Post(1, "", "", "")
        }
    },
    action: ActionTypes
) {
    switch (action.type) {
        case "SET_ALL_NEWS":
            // console.log(action.payload)
            return {

                ...state,
                posts: action.payload,

            }

        case "SET_LATEST_NEWS":

            // console.log({
            //     ...state,
            //     latestPost: action.payload
            // })
            return {
                ...state,
                latestPost: action.payload
            }
        default:
            return state;
    }
}


