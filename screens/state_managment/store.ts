import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
// import {newsReducer} from "./domain/reducer";
import {Store} from "./types";
import {ActionTypes} from "./domain/actions";
import newsReducer from "./domain/reducer";

const middleware = applyMiddleware(thunk);


export const store = createStore(combineReducers({
    newsReducer
}), middleware);
