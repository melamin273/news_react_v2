import {Post} from "../../models/Post";

export interface Store {
    newsReducer: {
        posts: Post[];
        latestPost: Post;
        newTodo: string;
    }

}
