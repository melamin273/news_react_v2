import {Post} from "../models/Post";

export type RootStackParamList = {
    MainHome: undefined;
    NewsDetail: { post: Post };
    Home : undefined
    Search : undefined
    Archive : undefined
    Category : undefined
};