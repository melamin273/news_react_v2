import React, {useContext, useEffect, useState} from "react";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
    ScrollView,
    StyleSheet,
    Text,
    View,
    Image,
    FlatList,
    TouchableHighlight,
    ProgressBarAndroid,
    Button
} from 'react-native'
import * as url from "url";
import {useNavigation, useRoute} from "@react-navigation/native";
import NewsDetailScreen from "../NewsDetailScreen";
import {PostServices} from "../../services/pos_services";
import {Post} from "../../models/Post";
import {StackNavigationProp} from "@react-navigation/stack";
import {RootStackParamList} from "../RootStackPrams";
import {ListTypeContext} from "../../App";
import {useDispatch, useSelector} from "react-redux";
import {Store} from "../state_managment/types";
import {getAllNews, getLatestNews} from "../state_managment/domain/actions";

let posService: PostServices = new PostServices()


const HomeFragment: React.FC<{}> = () => {
    return (
        <ScrollView>
            <TrendingNews/>
            <TodayReads/>
        </ScrollView>
    );

}

const ChangeListView: React.FC<{}> = () => {
    const [isRowView, setIsRowView] = useState(false);

    return (

        <TouchableHighlight
            underlayColor="white"
            style={[{marginHorizontal: 15}]}
            onPress={() => {
                setIsRowView(!isRowView)
            }}
        >
            <Icon name={isRowView ? "view-grid" : "format-list-bulleted"} size={30}/>
        </TouchableHighlight>

    );
}

const TrendingNews: React.FC<{}> = () => {
    // const [post, setPost] = useState(new Post(0, "", "", ""));

    let post = useSelector((state: Store) => state.newsReducer.latestPost)

    let state = useSelector((state: Store) => state)

    let dispatch = useDispatch()


    useEffect(() => {
        // console.log("OLDS DATA --->" + post)
        // console.log("OLDS STATE --->", state)

        dispatch(getLatestNews())
        // posService.getLatestPost().then((post: Post) => {
        //     setPost(post)
        // }).catch(err => {
        //     console.log(err)
        // })
    })

    // useEffect(() => {
    //
    //     console.log("NEW STATE --->", state)
    //
    //     console.log("NEW DATA --->" + post)
    // }, [post])

    return post != null ? (
        <View style={[styles.container, {flexDirection: "column"}]}>
            <Text style={[{color: "orange", paddingVertical: 5, fontSize: 10}]}>TRENDING</Text>
            <Text style={[{paddingVertical: 10, fontSize: 15}]}>{post.body}</Text>
            <Image source={{uri: post.imageUrl}}
                   style={{width: "100%", height: 250, borderRadius: 10}}/>

        </View>
    ) : <View/>
}


const TodayReadItem: React.FC<{ post: Post, isFull: boolean }> = ({post, isFull}) => {
    let navigation = useNavigation()


    return (
        <TouchableHighlight style={[{borderRadius: 10}]} onPress={() => {

            navigation.navigate('NewsDetail' as never, {"id": 11} as never)

        }} underlayColor="white">
            <View style={[{flex: 1, flexDirection: "column", padding: 10, width: !isFull ? 200 : "100%"}]}>

                <Image source={{uri: post.imageUrl}}
                       style={[{width: isFull ? "100%" : 180, height: 180, borderRadius: 20}]}/>
                <Text style={[{color: "orange"}]}>{post.title.substring(0, 20) + ".."}</Text>
                <Text>{post.body.substring(0, 20) + ".."}</Text>


            </View>
        </TouchableHighlight>

    );
}
const TodayReads: React.FC<{}> = () => {


    // const [posts, setData] = useState(new Array<Post>());
    const [isRow, setIsRow] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);
    const posts = useSelector((state: Store) => state.newsReducer.posts);
    const dispatch = useDispatch();

    useEffect(() => {
        let isMounted = true;
        // note mutable flag


        dispatch(getAllNews())
        // posService.getAllPosts().then((posts: Array<Post>) => {
        //     console.log(posts.length)
        //     setData(
        //         posts
        //     )
        //     setIsLoaded(true)
        // }).catch(err => {
        //     console.log(err)
        //     setIsLoaded(true)
        //
        // })
    })


    return (
        <ListTypeContext.Provider value={isRow}>

            <ListTypeContext.Consumer>
                {
                    (context) => (
                        <View style={[{flex: 1, flexDirection: "column", margin: 16}]}>


                            <View style={[{
                                flex: 1,
                                flexDirection: "row",
                                justifyContent: "space-between",
                                alignContent: "center"
                            }]}>
                                <Text style={[{marginHorizontal: 10, fontSize: 20}]}>Today Reads </Text>


                                <TouchableHighlight
                                    underlayColor="white"
                                    style={[{marginHorizontal: 15}]}
                                    onPress={() => {
                                        setIsRow(!isRow)
                                    }}
                                >
                                    <Icon name={isRow ? "view-grid" : "format-list-bulleted"} size={30}/>
                                </TouchableHighlight>

                            </View>
                            <FlatList<Post> data={posts}
                                            horizontal={context}
                                            showsHorizontalScrollIndicator={false}

                                            keyExtractor={item => item.id.toString()}

                                            renderItem={({item}) => <TodayReadItem post={item} isFull={!context}/>}/>
                        </View>)
                }
            </ListTypeContext.Consumer>
        </ListTypeContext.Provider>


    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
});

class News {
    title: string;
    body: string;
    imageUrl: string;

    constructor(title: string, body: string, imageUrl: string) {
        this.title = title;
        this.body = body;
        this.imageUrl = imageUrl;
    }
}


export default HomeFragment;
