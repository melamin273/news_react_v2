import React, {useEffect, useState} from 'react'
import {Text} from 'react-native'

import {NativeModules} from 'react-native';

const {CalculatorModule} = NativeModules

const CategoriesFragment: React.FC<{}> = () => {


    const [result, setResult] = useState(4)

    useEffect(() => {
        CalculatorModule.add(1, 2, (resultFromNative: number) => {
            setResult(resultFromNative)
        })
    })

    return (
        <Text>{result}</Text>
    )

}

export default CategoriesFragment
